﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;
namespace IMDB.Repository
{
    public class MovieRepo
    {
        public List<Movie> movies;
        public MovieRepo()
        {
            movies = new List<Movie>();
        }
        public void Add(Movie movie)
        {
            movies.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return movies.ToList();
        }
    }
}

