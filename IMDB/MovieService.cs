﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;
namespace IMDB
{
    public class MovieService
    {
        public MovieRepo movieRepository;
        public MovieService()
        {
            movieRepository = new MovieRepo();
        }
        public void AddMovie(string name, int yearOfRelease, string plot, List<Person> actors, Person producer)
        {
            movieRepository.Add(new Movie(name, yearOfRelease, plot, actors, producer));

        }
        public List<Movie> ListMovies()
        {
            return movieRepository.GetMovies();
        }
        public void DeleteMovie(int id)
        {
            var movie = ListMovies()[id - 1];
            movieRepository.movies.Remove(movie);
        }

    }
}
