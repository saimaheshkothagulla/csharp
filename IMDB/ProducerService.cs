﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;
namespace IMDB
{
    public class ProducerService
    {
        public ProducerRepo producerRepository;
        public ProducerService()
        {
            producerRepository = new ProducerRepo();
        }
        public void AddProducer(String name, string dateOfBirth)
        {
            var producer = new Person(name, dateOfBirth);
            producerRepository.Add(producer);
        }
        public List<Person> GetProducers()
        {
            return producerRepository.GetList();
        }
    }
}
