﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Person
    {
        public string Name { set; get; }
        public string DateOfBirth { set; get; }
        public Person(String name, string dob)
        {
            Name = name;
            DateOfBirth = dob;
        }
    }
}
