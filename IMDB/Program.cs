﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using IMDB.Domain;
using System.Globalization;
namespace IMDB
{
    class Program
    {

        public static void Main(string[] args)
        {
            ActorService actorService = new ActorService();
            ProducerService producerService = new ProducerService();
            MovieService movieService = new MovieService();



            int choice;
            while (true)
            {
                string options = "\n\nenter your Choice  :\n1.list movies\n2.add movie\n3.add actor\n4.add producer\n5.delete movie\n6.exit";
                Console.WriteLine(options);
                choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                switch (choice)
                {
                    case 1:
                        try
                        {
                            int movieID = 1;
                            int actorID;
                            if (movieService.ListMovies().Count == 0)
                            {
                                Console.WriteLine("No movies are available..");

                            }
                            else
                            {
                                Console.WriteLine("MovieId" + "  " + "Title" + "  " + "YearOfRelease" + "  " + "Plot");
                                Console.WriteLine("--------------------------------------");
                                foreach (var movie in movieService.ListMovies())
                                {
                                    Console.WriteLine(movieID + ". " + movie.Title + " " + movie.YearOfRelease + " " + movie.Plot);
                                    Console.WriteLine("\t->Actors:-");
                                    actorID = 0;
                                    foreach (var actor in movie.Actors)
                                    {
                                        actorID++;
                                        Console.WriteLine("\t\t" + actorID + "." + actor.Name + " " + actor.DateOfBirth);
                                    }
                                    Console.WriteLine("\t->producer:-");
                                    Console.WriteLine("\t" + movie.Producer.Name + " " + movie.Producer.DateOfBirth);
                                    movieID++;

                                }
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        break;
                    case 2:
                        try
                        {
                            string movieName;
                            int yearOfRelease;
                            string plot;
                            int[] userActorIDs;
                            int userProducerID;
                            if (actorService.GetActors().Count == 0)
                            {
                                throw new Exception("No actors are available..");
                            }
                            if (producerService.GetProducers().Count == 0)
                            {
                                throw new Exception("No producers are available..");
                            }
                            Console.Write("Name of the movie: ");
                            movieName = Console.ReadLine().Trim();

                            while (string.IsNullOrEmpty(movieName) || movieName.Length < 1)
                            {
                                Console.WriteLine("your entered name is invalid.name should not be null/empty..");
                                Console.Write("please ,enter Name of movie: ");
                                movieName = Console.ReadLine();
                            }
                            Console.Write("enter year of Release: ");
                            yearOfRelease = Convert.ToInt32(Console.ReadLine());
                            DateTime now = DateTime.Now;
                            int presentYear = now.Year;
                            if (yearOfRelease > presentYear)
                            {
                                throw new Exception("Year must be valid ..");
                            }
                            Console.Write("enter Plot :");
                            plot = Console.ReadLine();
                            int actorID = 1, producerID = 1;


                            Console.WriteLine("choose actors from below: ");
                            foreach (var actor in actorService.GetActors())
                            {
                                Console.WriteLine(actorID + " " + actor.Name + " " + actor.DateOfBirth);
                                actorID += 1;
                            }
                            userActorIDs = Array.ConvertAll(Console.ReadLine().Split(' '), s => int.Parse(s));

                            List<Person> actors = new List<Person>();
                            foreach (var id in userActorIDs)
                            {
                                actors.Add(actorService.GetActors()[id - 1]);
                            }

                            if (producerService.GetProducers().Count == 0)
                            {
                                throw new Exception("no producers are available..");
                            }
                            Console.WriteLine("choice producers from below: ");
                            foreach (var prod in producerService.GetProducers())
                            {
                                Console.WriteLine(producerID + " " + prod.Name + " " + prod.DateOfBirth);
                                producerID += 1;

                            }
                            userProducerID = Convert.ToInt32(Console.ReadLine());
                            movieService.AddMovie(movieName, yearOfRelease, plot, actors, producerService.GetProducers()[userProducerID - 1]);
                            Console.WriteLine("movie added successfully....");

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 3:
                        try
                        {
                            string actorName;
                            string actorDOB;

                            Console.Write("enter Name of actor: ");
                            actorName = Console.ReadLine().Trim();
                            while (string.IsNullOrEmpty(actorName) || actorName.Length < 1)
                            {
                                Console.WriteLine("your enterted name is invalid.name should not be null/empty.");
                                Console.Write("please ,enter Name of actor: ");
                                actorName = Console.ReadLine();
                            }
                            Console.Write("enter date of birth in format(DD-MM-YYYY):");
                            actorDOB = Convert.ToDateTime(DateTime.ParseExact(Console.ReadLine().Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToString("dd-MM-yyyy"); ;
                            actorService.AddActor(actorName, actorDOB);
                            Console.WriteLine("actor added successfully....");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 4:
                        try
                        {
                            string producerName;
                            string producerDOB;
                            Console.Write("enter Name of Producer: ");
                            producerName = Console.ReadLine().Trim();
                            while (string.IsNullOrEmpty(producerName) || producerName.Length < 1)
                            {
                                Console.WriteLine("your enterted name is invalid.name should not be null/empty.");
                                Console.Write("please ,enter Name of actor: ");
                                producerName = Console.ReadLine();
                            }

                            Console.Write("enter date of birth in format(DD-MM-YYYY): ");
                            producerDOB = Convert.ToDateTime(DateTime.ParseExact(Console.ReadLine().Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToString("dd-MM-yyyy"); ;
                            producerService.AddProducer(producerName, producerDOB);
                            Console.WriteLine("producer added successfully....");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 5:
                        try
                        {
                            int movieID = 1, actorID;
                            int NoOfMoviesAvailable = movieService.ListMovies().Count;
                            if (NoOfMoviesAvailable == 0)
                            {
                                throw new Exception("no movies are availabe to delete..");
                            }
                            Console.WriteLine("Movies list:-");
                            actorID = 0;
                            Console.WriteLine("MovieId" + "  " + "Title" + "  " + "YearOfRelease" + "  " + "Plot");
                            Console.WriteLine("--------------------------------------");
                            foreach (var movie in movieService.ListMovies())
                            {
                                Console.WriteLine(movieID + ". " + movie.Title + " " + movie.YearOfRelease + " " + movie.Plot);
                                Console.WriteLine("\t->Actors:-");

                                foreach (var actor in movie.Actors)
                                {
                                    actorID++;
                                    Console.WriteLine("\t\t" + actorID + "." + actor.Name + " " + actor.DateOfBirth);
                                }
                                Console.WriteLine("\t->producer:-");
                                Console.WriteLine("\t" + movie.Producer.Name + " " + movie.Producer.DateOfBirth);
                                movieID++;

                            }
                            Console.WriteLine("--------------------------------------\n");
                            Console.Write("Enter ID of movie to delete:-");
                            int ID = Convert.ToInt32(Console.ReadLine());
                            if (ID > NoOfMoviesAvailable && ID <= 0)
                            {
                                throw new Exception("Invalid Id..");
                            }

                            movieService.DeleteMovie(ID);
                            Console.WriteLine("successfully deleted movie..");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("enter valid choice..");
                        break;



                }
            }



        }
    }
}

