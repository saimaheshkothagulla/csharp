﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Person> Actors { get; set; }
        public Person Producer { get; set; }
        public Movie(string name, int yearOfRelease, string plot, List<Person> actors, Person producer)
        {
            Title = name;
            YearOfRelease = yearOfRelease;
            Plot = plot;
            Actors = actors;
            Producer = producer;
        }
    }
}
