﻿using System;
using System.Collections.Generic;
using IMDB;
using IMDB.Domain;
using IMDB.Repository;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using NUnit.Framework;
namespace IMDB.Test
{
    [Binding]
    public class ConsoleApplicationForIMDBSteps
    {
        private List<Person> _actors=new List<Person>();
        private Person _producer;
        private List<Movie> _movies;
        private ActorService _actorService;
        private ProducerService _producerService;
        private MovieService _movieService;
        private string _name, _plot;
        private int _year;

        public ConsoleApplicationForIMDBSteps()
        {

            _actorService = new ActorService();
            _producerService = new ProducerService();
            _movieService = new MovieService();


        }




        [Given(@"I have movie with name ""(.*)""")]
        public void GivenIHaveMovieWithName(string name)
        {
            _name = name;
        }

        [Given(@"Year of release is ""(.*)""")]
        public void GivenYearOfReleaseIs(int year)
        {
            _year = year;
        }

        [Given(@"Plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"Actor is ""(.*)""")]
        public void GivenActorIs(string actorsId)
        {
            var ActorsIDArray = Array.ConvertAll(actorsId.Split(' '), s => int.Parse(s));
            foreach (var id in ActorsIDArray)
            {
                _actors.Add(_actorService.GetActors()[id - 1]);
            }
        }

        [Given(@"producer is ""(.*)""")]
        public void GivenProducerIs(int producerID)
        {
            _producer = _producerService.GetProducers()[producerID - 1];

        }
        [When(@"i add the movie into IMDB")]
        public void WhenIAddTheMovieIntoIMDB()
        {

            _movieService.AddMovie(_name, _year, _plot, _actors, _producer);



        }
        [Then(@"IMBD would looks like below")]
        public void ThenIMBDWouldLooksLikeBelow(Table table)
        {
            var movieTable = table.CreateSet<(string title, int year, string plot, string actors, string producer)>();
            var movies = _movieService.ListMovies();
            int movieID = 0;
            foreach (var movie in movieTable)
            {
                var movieDetails = movies[movieID];
                Assert.AreEqual(movie.title, movieDetails.Title);

                Assert.AreEqual(movie.year, movieDetails.YearOfRelease);
                Assert.AreEqual(movie.plot, movieDetails.Plot);
                var actors = movie.actors.Split(",");
                int actorID = 0;
                foreach (var actor in actors)
                {
                    Assert.AreEqual(actor, movieDetails.Actors[actorID].Name);
                    actorID++;
                }
                Assert.AreEqual(movie.producer, movieDetails.Producer.Name);
                movieID++;
            }


        }
        [Given(@"I have  a IMDB with movies")]
        public void GivenIHaveAIMDBWithMovies()
        {

        }

        [When(@"i fetch all movies")]
        public void WhenIFetchAllMovies()
        {
            _movies = _movieService.ListMovies();
        }
        [Then(@"IMDB would show all following movies")]
        public void ThenIMDBWouldShowAllFollowingMovies(Table table)
        {
            var movieTable = table.CreateSet<(string title, int year, string plot, string actors, string producer)>();
            var movies = _movieService.ListMovies();
            int movieID = 0;
            foreach (var movie in movieTable)
            {
                var movieDetails = movies[movieID];
                Assert.AreEqual(movie.title, movieDetails.Title);
                Assert.AreEqual(movie.year, movieDetails.YearOfRelease);
                Assert.AreEqual(movie.plot, movieDetails.Plot);
                var actors = movie.actors.Split(",");
                int actorID = 0;
                foreach (var actor in actors)
                {
                    Assert.AreEqual(actor, movieDetails.Actors[actorID].Name);
                    actorID++;
                }
                Assert.AreEqual(movie.producer, movieDetails.Producer.Name);
                movieID++;
            }
        }
        [BeforeScenario("addMovies")]
        public void AddingMovieToTheIMDB()
        {

            _actorService.AddActor("maheshbabu", "12-10-1981");
            _actorService.AddActor("samantha", "10-11-1991");
            _producerService.AddProducer("dil raju", "09-09-1971");
            _actorService.AddActor("Evans", "08-10-1979");
            _actorService.AddActor("Hemsworth", "19-10-1991");
            _actorService.AddActor("robert", "29-10-1987");
            _producerService.AddProducer("kevin", "21-08-1981");



        }
        [BeforeScenario("listMovies")]
        public void ListingAllMoviesOfIMDB()
        {

            var actor1 = new Person("maheshbabu", "12-10-1981");
            var actor2 = new Person("samantha", "10-11-1991");
            var producer1 = new Person("dil raju", "09-09-1971");
            var actor3 = new Person("Evans", "08-10-1979");
            var actor4 = new Person("Hemsworth", "19-10-1991");
            var actor5 = new Person("robert", "29-10-1987");
            var producer2 = new Person("kevin", "21-08-1981");
            _actorService.AddActor("maheshbabu", "12-10-1981");
            _actorService.AddActor("samantha", "10-11-1991");
            _producerService.AddProducer("dil raju", "09-09-1971");
            _actorService.AddActor("Evans", "08-10-1979");
            _actorService.AddActor("Hemsworth", "19-10-1991");
            _actorService.AddActor("robert", "29-10-1987");
            _producerService.AddProducer("kevin", "21-08-1981");



            _movieService.AddMovie("Avengers Endgame", 2019, "best action movie", new List<Person> { actor3, actor4, actor5 }, producer2);
            _movieService.AddMovie("Maharshi", 2019, "best movie", new List<Person> { actor1, actor2 }, producer1);


        }



    }
}






