﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ActorRepo
    {
        public List<Person> actors;

        public ActorRepo()
        {
            actors = new List<Person>();
        }
        public void Add(Person actor)
        {
            actors.Add(actor);
        }
        public List<Person> GetList()
        {
            return actors.ToList();
        }
    }
}
