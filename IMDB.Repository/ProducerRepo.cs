﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;
namespace IMDB.Repository
{
    public class ProducerRepo
    {
        public List<Person> producers;
        public ProducerRepo()
        {
            producers = new List<Person>();
        }
        public void Add(Person producer)
        {
            producers.Add(producer);
        }
        public List<Person> GetList()
        {
            return producers.ToList();
        }
    }
}
