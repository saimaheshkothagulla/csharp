﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;
namespace IMDB
{
    public class ActorService
    {
        public ActorRepo actorRepository;
        public ActorService()
        {
            actorRepository = new ActorRepo();

        }
        public void AddActor(String name, string dateOfBirth)
        {
            var actor = new Person(name, dateOfBirth);
            actorRepository.Add(actor);
        }
        public List<Person> GetActors()
        {
            return actorRepository.GetList();
        }

    }
}
