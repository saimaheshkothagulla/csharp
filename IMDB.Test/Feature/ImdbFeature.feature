﻿Feature: Console Application for IMDB
		adding and listing movies from IMDB 

@addMovies
Scenario: Adding movie to the IMDB
	Given I have movie with name "Maharshi"
	And Year of release is "2019"
	And Plot is "best movie" 
	And Actor is "1 2"
	And producer is "1"
	When i add the movie into IMDB
	Then IMBD would looks like below
	| Movie            | Year of release | Plot              | Actor                  | Producers |
	| Maharshi         | 2019            | best movie        | maheshbabu,samantha    | dil raju  |


@listMovies
Scenario: listing all movies of IMDB
	Given I have  a IMDB with movies
	When i fetch all movies
	Then IMDB would show all following movies
	| Movie            | Year of release | Plot              | Actor                  | Producers |
	| Avengers Endgame | 2019            | best action movie | Evans,Hemsworth,robert | kevin     |
	| Maharshi         | 2019            | best movie        | maheshbabu,samantha    | dil raju  |